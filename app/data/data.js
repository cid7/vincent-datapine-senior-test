var datas = [

	{
		id: "5165",
		title: "some editable title",
		description: "some description here",
		data : [
			{
				value: 30,
				color:"#F7464A",
				highlight: "#FF5A5E",
				label: "Red"
			},
			{
				value: 540,
				color: "#46BFBD",
				highlight: "#5AD3D1",
				label: "Green"
			},
			{
				value: 170,
				color: "#FDB45C",
				highlight: "#FFC870",
				label: "Yellow"
			}
		]	
	},
	{
		id: "5645",
		title: "another editable title",
		description: "some description here",
		data : [
			{
				value: 350,
				color:"#F7464A",
				highlight: "#FF5A5E",
				label: "Red"
			},
			{
				value: 40,
				color: "#46BFBD",
				highlight: "#5AD3D1",
				label: "Green"
			},
			{
				value: 500,
				color: "#FDB45C",
				highlight: "#FFC870",
				label: "Yellow"
			}
		]	
	},
	{
		id: "2411",
		title: "some title here too",
		description: "some very very very long description here",
		data : [
			{
				value: 100,
				color:"#F7464A",
				highlight: "#FF5A5E",
				label: "Red"
			},
			{
				value: 222,
				color: "#46BFBD",
				highlight: "#5AD3D1",
				label: "Green"
			},
			{
				value: 33,
				color: "#FDB45C",
				highlight: "#FFC870",
				label: "Yellow"
			}
		]	
	},
	{
		id: "5345",
		title: "some editable title",
		description: "some description here",
		data : [
			{
				value: 300,
				color:"#F7464A",
				highlight: "#FF5A5E",
				label: "Red"
			},
			{
				value: 50,
				color: "#46BFBD",
				highlight: "#5AD3D1",
				label: "Green"
			},
			{
				value: 100,
				color: "#FDB45C",
				highlight: "#FFC870",
				label: "Yellow"
			}
		]	
	}
	

]