	  /*
	   * Chart View
	   * @param {object} see data.json
	   */

	var ChartView = Backbone.View.extend({
			
		initialize: function(options){
			
			this.$el = $($('.gridster li.empty')[0]);
			
			if (!this.$el.length){
				return;
			}

			this.id = options.id;
			this.model = options.data;
			
			this.$el.removeClass("empty");
			this.template = _.template($('#chart').html());
			this.$el.append(this.template({chart: options}));
			
			this.listenTo(Event, "render"+ this.id, this.render);
			
			this.render();
			
		},
		
		render: function(){
		
			this.chart = this.$el.find('canvas');

			if (this.myNewChart )
				this.myNewChart.destroy();	
				
			var newChartHeight = this.$el.height() - this.$el.find('.title').height() - this.$el.find('.description').height()-10;
			this.chart.css('width',this.$el.width() );
			this.chart.css('height',newChartHeight);	
			ctx = this.chart.get(0).getContext("2d");
			this.myNewChart = new Chart(ctx).PolarArea(this.model);

		}
	});	
			
	  /*
	   * Abstract Screen View 
	   */
	   
	var AbstractScreenView = Backbone.View.extend({
		
		initialize: function(){
			
			this.listenTo(Event, "displayScreen", this.onScreenClicked);
		},
		
		onScreenClicked(target){
			this.$el.hide();
			if(target === this.screenName)
				this.$el.show();
		}
	});
	
  /*
   * Grid View
   */
	var GridView = AbstractScreenView.extend({
		
		screenName: "home",
		initialize: function(){
			
			AbstractScreenView.prototype.initialize.call(this);
			this.$el = $("<div class='gridView screen'></div>");
			this.template = _.template($('#grid').html());
			this.$el.append(this.template);
			
			this.render();
		},

		render: function(widget){
			var that = this;
			
			$('body').append(this.$el);
			
			gridster = $(".gridster ul").gridster({
			
				widget_base_dimensions: [100, 100],
				widget_margins: [5, 5],
				resize: {
					enabled: true,
					stop: function(e, ui, $widget) {
					
						Event.trigger( "render" + $widget.find('canvas').attr('id'))
					}

				}
			}).data('gridster');
		
		}
	});
	
  /*
   * howTo View
   */
	var HowToView = AbstractScreenView.extend({
		
		screenName: "howto",
		initialize: function(){
			
			AbstractScreenView.prototype.initialize.call(this);
			this.$el = $("<div class='howToView screen' hidden></div>");
			this.template = _.template($('#howTo').html());
			this.$el.append(this.template);
	
			this.render();
		},
		
		render: function(){
			$('body').append(this.$el);
		}
	});
		
  /*
   * Menu View
   */
	var MenuView = Backbone.View.extend({
		
		events: {
			"click .clickable": "onLinkClicked"
		},
		
		initialize: function(){
			
			this.$el = $("<div class='menuView'></div>");
			this.template = _.template($('#menu').html());
			this.$el.append(this.template);

			this.render();
		},
	
		onLinkClicked(e){

			var target = $(e.currentTarget).attr('target');
			
			this.$el.find("li").removeClass("active");
			$(e.currentTarget).addClass("active");
			$(".screen").hide();
	
			Event.trigger( "displayScreen",  target)
		},
		
		render: function(){
			$('body').append(this.$el);
			this.delegateEvents();
		}
	});
	